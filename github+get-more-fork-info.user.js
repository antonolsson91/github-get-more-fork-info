// ==UserScript==
// @name         Github - Get More Fork Info
// @namespace    https://bitbucket.org/antonolsson91/github-get-more-fork-info/
// @version      1.0.1
// @description  1.0.0 => Show more fork information, specifically if the fork is ahead or behind or even with the forked repository. 1.0.1 => Fixes broken script due to HTML changes. Adds API requests. Adds some styling.
// @author       Anton
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// @match        https://github.com/*/network/members
// @grant        none
// @run-at       document-end
// @updateURL    https://bitbucket.org/antonolsson91/github-get-more-fork-info/raw/HEAD/github%2Bget-more-fork-info.user.js
// ==/UserScript==

window.stop = false;
var counter = 0;
const countto = 0;

// Default username for authenticating API requests
let authUser = "eagl3s1ght";

async function fetchRepo(author, repoName){
    return new Promise((resolve, reject, next) => {
        fetch(`https://api.github.com/repos/${author}/${repoName}`, {  headers: { 'Authorization': authUser,"Accept": "application/vnd.github.v3+json" } }).then(res => { return res.json() }).then((res) => {
            try{
                resolve( res );
            } catch(e) {
                window.stop = true; console.error(e);
                reject( e );
            }


        })

    })
}
async function fetchForks(author, repoName, sortString){
    return new Promise((resolve, reject, next) => {
        fetch(`https://api.github.com/repos/${author}/${repoName}/forks?${sortString}`, {  headers: { 'Authorization': authUser,"Accept": "application/vnd.github.v3+json" } }).then(res => { return res.json() }).then((res) => {
            try{
                resolve( res );
            } catch(e) {
                window.stop = true; console.error(e);
                reject( e );
            }


        })

    })
}


function handleRes(repoLink){

    let author = repoLink.split('/')[1];
    let infobarhtml = localStorage.getItem(repoLink).replace("behind","<strong>behind</strong>").replace("ahead","<strong>ahead</strong>").replace("even","<strong>even</strong>")
    let spanElem = $(`<span class="">${infobarhtml}</span>`);

    let gmfiSelector = `.${author} .gmfi`; let gmfi = $(gmfiSelector);
    gmfi.append(spanElem)

    let $p = $(`.${author} > a`).parent()
    $p.append(`<small class="extra-info" style="font-size: 80%;>- ${infobarhtml}</small>`)


    if( infobarhtml.includes("ahead") && infobarhtml.includes("behind"))
    {
        $p.addClass('toggle-behind-ahead');
        spanElem.append(`⬆️⬇️`);
    }
    if( infobarhtml.includes("ahead") && false == infobarhtml.includes("behind"))
    {
        $p.addClass('toggle-ahead');
        spanElem.append(`⬆️`);
    }
    if( infobarhtml.includes("even"))
    {
        $p.addClass('toggle-even');
        spanElem.append(`➡️`);
    }
    if( infobarhtml.includes("behind") && false == infobarhtml.includes("ahead"))
    {
        $p.addClass('toggle-behind');
        spanElem.append(`⬇️`);
    }
}


(function($) {
    'use strict';

    $(document).ready(async ()=>{

        authUser = $(".Header-link img.avatar").attr("alt").replace("@","");


        // setup filter
        let filters = $(`
<style>.repo:nth-child(even) {
    background: rgba(0,0,0,0.05);
}</style>

<div id="filters">
<a href="#" class="btn btn-sm mr-2" id="toggle-even"><span class="toggle-word">hide</span> even</a>
<a href="#" class="btn btn-sm mr-2" id="toggle-behind"><span class="toggle-word">hide</span> behind</a>
<a href="#" class="btn btn-sm mr-2" id="toggle-ahead"><span class="toggle-word">hide</span> ahead</a>
<a href="#" class="btn btn-sm mr-2" id="toggle-behind-ahead"><span class="toggle-word">hide</span> behind & ahead</a>
</div>`)

        $('#network').prepend(filters)

        $('.network').on("click", "#toggle-behind,#toggle-even,#toggle-ahead,#toggle-behind-ahead", e => {
            let $e = $(e.target)
            let labelElem = $e.find('.toggle-word');
            console.log($e)
            $e.toggleClass("toggled")
            $('.' + $e.attr("id")).toggle()

            if( $('.' + $e.attr("id")).is(":visible") ){
                labelElem.html("hide")
            } else {
                labelElem.html("show")
            }

            e.preventDefault()
            return false
        })



        $('.repo').each(async (i, e) => {

            let $e = $(e);
            let repoAuthor = $e.find('a[data-hovercard-type="user"]').text().trim()
            let repoName = $e.find('a:last-child').text().trim()

            // const repo = await fetchRepo(repoAuthor, repoName); console.log("repo for", repoAuthor, "with repo", repoName," fetched:", repo);

            $e.addClass(repoAuthor);

            counter++;
            if(counter>countto && countto != 0) return false;
            let repoLink = $(e).find('a:last-child').attr('href')

            $e.append(`<span class="gmfi float-right border rounded-1 flex-shrink-0 bg-gray px-1 text-gray-light ml-1"></span>`);

            if(typeof localStorage.getItem(repoLink) == "string" && localStorage.getItem(repoLink) != false){
                handleRes(repoLink)
                //$(e).append(`<div>found cache</div>`)
            } else {
                if( window.stop == true ){ console.warn("we got error, not grabbing cache"); return false };
                //$(e).append(`<div>grabbing cache for ${repoLink}</div>`)



                fetch(repoLink).then(res => { return res.text() }).then((res) => {
                    try{
                        //console.log("res.length = ", res.length)

                        let $temp = $(`<div id="temp">${res}</div>`)
                        let infobar = $temp.find('.branch-infobar')
                        let x = $($temp).find("div").filter(function(){ return $(this).text().toLowerCase().includes("This branch is".toLowerCase());}).last()

                        let infobarhtml = x.text().trim()

                        console.log(`${repoLink} - ${infobarhtml}`, { "ahead" : infobarhtml.includes('ahead'), "even" : infobarhtml.includes('even'), "behind": infobarhtml.includes('behind') })


                        localStorage.setItem(repoLink, infobarhtml)
                        //$(e).append(`<div>cached. ${localStorage.getItem(repoLink).length}</div>`);
                        handleRes(repoLink)

                    } catch(e) {
                        window.stop = true; console.error(e);
                    }


                })
            }


        }) // each .fork


        const author = $('.author').text().trim()
        const repoName = $('strong[itemprop="name"]').text().trim()

        console.log(author, repoName)


        const repo = await fetchRepo(author, repoName)

        console.log("repo", repo);


        const forks = await fetchForks(author, repoName, "page=1&per_page=100&sort=stargazers")

        console.log("First fork", forks[0], "retrieved", forks.length);
        const forkIcon = `<svg class="octicon octicon-repo-forked" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M5 3.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm0 2.122a2.25 2.25 0 10-1.5 0v.878A2.25 2.25 0 005.75 8.5h1.5v2.128a2.251 2.251 0 101.5 0V8.5h1.5a2.25 2.25 0 002.25-2.25v-.878a2.25 2.25 0 10-1.5 0v.878a.75.75 0 01-.75.75h-4.5A.75.75 0 015 6.25v-.878zm3.75 7.378a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm3-8.75a.75.75 0 100-1.5.75.75 0 000 1.5z"></path></svg>`;
        const starIcon = `<svg class="octicon octicon-star" height="16" viewBox="0 0 16 16" version="1.1" width="16" aria-hidden="true"><path fill-rule="evenodd" d="M8 .25a.75.75 0 01.673.418l1.882 3.815 4.21.612a.75.75 0 01.416 1.279l-3.046 2.97.719 4.192a.75.75 0 01-1.088.791L8 12.347l-3.766 1.98a.75.75 0 01-1.088-.79l.72-4.194L.818 6.374a.75.75 0 01.416-1.28l4.21-.611L7.327.668A.75.75 0 018 .25zm0 2.445L6.615 5.5a.75.75 0 01-.564.41l-3.097.45 2.24 2.184a.75.75 0 01.216.664l-.528 3.084 2.769-1.456a.75.75 0 01.698 0l2.77 1.456-.53-3.084a.75.75 0 01.216-.664l2.24-2.183-3.096-.45a.75.75 0 01-.564-.41L8 2.694v.001z"></path></svg>`;
        const watchIcon = `<svg class="octicon octicon-eye" height="16" viewBox="0 0 16 16" version="1.1" width="16" aria-hidden="true"><path fill-rule="evenodd" d="M1.679 7.932c.412-.621 1.242-1.75 2.366-2.717C5.175 4.242 6.527 3.5 8 3.5c1.473 0 2.824.742 3.955 1.715 1.124.967 1.954 2.096 2.366 2.717a.119.119 0 010 .136c-.412.621-1.242 1.75-2.366 2.717C10.825 11.758 9.473 12.5 8 12.5c-1.473 0-2.824-.742-3.955-1.715C2.92 9.818 2.09 8.69 1.679 8.068a.119.119 0 010-.136zM8 2c-1.981 0-3.67.992-4.933 2.078C1.797 5.169.88 6.423.43 7.1a1.619 1.619 0 000 1.798c.45.678 1.367 1.932 2.637 3.024C4.329 13.008 6.019 14 8 14c1.981 0 3.67-.992 4.933-2.078 1.27-1.091 2.187-2.345 2.637-3.023a1.619 1.619 0 000-1.798c-.45-.678-1.367-1.932-2.637-3.023C11.671 2.992 9.981 2 8 2zm0 8a2 2 0 100-4 2 2 0 000 4z"></path></svg>`

        forks.forEach(fork => {
            const { full_name, forks, forks_count, size, name, pushed_at, watchers, watchers_url, watchers_count, updated_at, created_at, forks_url, stargazers_count, stargazers_url, html_url } = fork;
            const forkAuthor = full_name.split("/")[0];

let gmfiSelector = `.${forkAuthor} .gmfi`; let gmfi = $(gmfiSelector);

            gmfi.append(`

<span class="border rounded-1 flex-shrink-0 bg-gray px-1 text-gray-light ml-1" >
 <span class="bg-gray-500" >${forkIcon}</span>
 <span class="bg-gray-500" ><a href="${forks_url}">${forks_count}</a></span>
</span>
<span class="border rounded-1 flex-shrink-0 bg-gray px-1 text-gray-light ml-1 " >
 <span class="bg-gray-500" >${starIcon}</span>
 <span class="bg-gray-500" ><a href="${stargazers_url}">${stargazers_count}</a></span>
</span>

<span class="border rounded-1 flex-shrink-0 bg-gray px-1 text-gray-light ml-1 " >
 <span class="bg-gray-500" >${watchIcon}</span>
 <span class="bg-gray-500" ><a href="${watchers_url}">${watchers_count}</a></span>
</span>

<abbr class="bg-white"  title="Created: ${created_at}
Last push: ${pushed_at}
Last updated: ${updated_at}">?</abbr>

`)
        })



    })
})(jQuery);