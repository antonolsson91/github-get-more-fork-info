# github-get-more-fork-info

When viewing the network/forks of a repository, it's hard to get a glance of who worked a lot on their fork.
This userscript solves that problem by scraping every fork for information about commits.

![https://bitbucket.org/antonolsson91/github-get-more-fork-info/raw/HEAD/showcase.png](https://bitbucket.org/antonolsson91/github-get-more-fork-info/raw/HEAD/showcase.png)

## installation

1. Download [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) or any other userscript handler.
2. View [github+get-more-fork-info.user.js](https://bitbucket.org/antonolsson91/github-get-more-fork-info/raw/HEAD/github%2Bget-more-fork-info.user.js) in its raw form.
3. Accept the installation prompt.

## usage

Visit any repository network, e.g. [https://github.com/pikapkg/web/network/members](pikapkg/web) by clicking the number next to "Fork" button.

The userscript acts on the following URL: https://github.com/*/network/members

The data scraped is cached in the browsers localStorage, and so it is only meant to be used once to get a good overlook of the repository forks.

## disclaimer

There's gonna be bugs. This is a pet project which I don't spend too much effort on, it's just a fancy little addition to the greatness of Github.
